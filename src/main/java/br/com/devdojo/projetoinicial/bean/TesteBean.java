package br.com.devdojo.projetoinicial.bean;

import br.com.devdojo.projetoinicial.annotations.Transactional;
import br.com.devdojo.projetoinicial.persistence.daointerfaces.DAO;
import br.com.devdojo.projetoinicial.persistence.model.Projeto;
import br.com.devdojo.projetoinicial.persistence.model.enums.Condition;
import com.google.common.collect.ImmutableMap;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

import static br.com.devdojo.projetoinicial.utils.FacesUtils.addErrorMessage;
import static br.com.devdojo.projetoinicial.utils.FacesUtils.addSuccessMessage;
import static java.util.Arrays.asList;

/**
 * Created by William Suane on 9/28/2016.
 */

@Named
@ViewScoped
public class TesteBean implements Serializable {
    private final DAO<Projeto> dao;
    private Projeto projeto;
    private List<Projeto> projetos;

    @Inject
    public TesteBean(DAO<Projeto> dao) {
        this.dao = dao;
    }

    @PostConstruct
    public void init(){
        projetos = dao.listAll();

        List<Projeto> projetoList = dao.findByAttributes(ImmutableMap.of("nome", "Projeto 1","id", 1L), asList(Condition.LIKE,Condition.EQUAL));
        System.out.println(projetoList);
    }
    @Transactional
    public void construct() throws Exception {
        try {
//            Projeto p = new Projeto();
//            Projeto p2 = new Projeto();
//            p.setNome("Projeto 1");
//            p2.setNome("Projeto 2");
//            dao.save(p);
//            dao.save(p2);
            System.out.println(projeto);
            addSuccessMessage("sucesso");
        } catch (Exception e) {
            e.printStackTrace();
            addErrorMessage("error");
            throw e;
        }
    }

    public Projeto getProjeto() {
        return projeto;
    }

    public void setProjeto(Projeto projeto) {
        this.projeto = projeto;
    }

    public List<Projeto> getProjetos() {
        return projetos;
    }

    public void setProjetos(List<Projeto> projetos) {
        this.projetos = projetos;
    }
}
