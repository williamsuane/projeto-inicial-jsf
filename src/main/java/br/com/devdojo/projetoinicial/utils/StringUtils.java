package br.com.devdojo.projetoinicial.utils;

/**
 * Created by William Suane on 9/29/2016.
 */
public class StringUtils {
    public static String likeLower(String param) {
        return "%" + param.toLowerCase() + "%";
    }
    public static String like(String param) {
        return "%" + param + "%";
    }
}
